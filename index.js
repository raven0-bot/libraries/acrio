const fs = require('fs');

const acrio = {}

acrio.createLog = function createLog(name, content) {
	if (!name) { throw new Error('[Acrio]: It is not possible to create a log with an empty name.'); }
	else if (!content) { throw new Error('[Acrio]: You cannot create an empty log.'); }
	else {
		const stream = fs.createWriteStream(`${name}.log`, { flags:'a' });
		stream.write(content + '\n');
	}
};
acrio.readLog = function readLog(name) {
    if (!name) throw new Error('[Acrio]: No log to read');
    else fs.readFile(`${name}.log`, (err, data) => {
        if (!data) return console.log('[Acrio]: No data was in the file, File isn\'t a .log file or doesn\'t exist');

        console.log(data.toString());
        });
}

module.exports = acrio